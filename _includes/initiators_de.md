---
Initiatoren:

   * [Jorgo Ananiadis](https://twitter.com/JorgoA) (Politiker und Netzaktivist)
   * [Kai Bösefeldt](https://twitter.com/kbosefeldt) (Software-Entwickler, Initiator des [Straightway-Projektes](https://straightway.github.io))
   * [Dr. Patrick Breyer](https://www.patrick-breyer.de) (Mitglied des [Europaparlaments](https://www.europarl.europa.eu))
   * [Anke Domscheit-Berg](https://mdb.anke.domscheit-berg.de) (Mitglied des [Deutschen Bundestages](https://www.bundestag.de))
   * [Saskia Esken](https://www.saskiaesken.de) (Mitglied des [Deutschen Bundestages](https://www.bundestag.de))
   * [Adrienne Fichter](https://www.republik.ch/~adriennefichter) (Journalistin)
   * [lic. iur. Viktor Györffy](https://www.psg-law.ch/partner/lic._iur._viktor_gyoerffy.html) (Rechtsanwalt)
   * [Michael Hausding](https://twitter.com/mhausding) (Vorstand [Internet Society Switzerland](https://www.isoc.ch))
   * [Prof. Dr. Andrea Herrmann](https://www.akad.de/fernstudium/akad-university/akad-forum-2019/speaker/dr-habil-andrea-herrmann/) (Professor an der [AKAD University](https://www.akad.de); [Herrmann & Ehrlich](http://www.herrmann-ehrlich.de))
   * [Hernâni Marques](https://vecirex.net) (Stiftungsrat [p≡p foundation](https://pep.foundation))
   * [Piratenpartei CH](https://www.piratenpartei.ch/)
   * [Peter Schaar](https://peter-schaar.de) (Vorsitzender der [Europäischen Akademie für Informationsfreiheit und Datenschutz (EAID)](https://www.eaid-berlin.de), Berlin; [Bundesbeauftragter für den Datenschutz und die Informationsfreiheit](https://www.bfdi.bund.de) a.D. (2003-2013))
   * [#TEAMHUMAN](https://teamhuman.ch)
   * [Marina Weisband](https://aula-blog.website/team/marina-weisband) (Publizistin, Künstlerin)
   * [Peter Welchering](https://www.welchering.de) (Journalist)
