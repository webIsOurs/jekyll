---
Initiators:

   * [Jorgo Ananiadis](https://twitter.com/JorgoA) (Politician and Internet Activist)
   * [Kai Bösefeldt](https://twitter.com/kbosefeldt) (Software Developer, Initiator of the [Straightway-Projektes](https://straightway.github.io))
   * [Dr. Patrick Breyer](https://www.patrick-breyer.de) (Member of the [European Parliament](https://www.europarl.europa.eu))
   * [Anke Domscheit-Berg](https://mdb.anke.domscheit-berg.de) (Member of the [German Parliament](https://www.bundestag.de))
   * [Saskia Esken](https://www.saskiaesken.de) (Member of the [German Parliament](https://www.bundestag.de))
   * [Adrienne Fichter](https://www.republik.ch/~adriennefichter) (Journalist)
   * [lic. iur. Viktor Györffy](https://www.psg-law.ch/partner/lic._iur._viktor_gyoerffy.html) (Lawyer)
   * [Michael Hausding](https://twitter.com/mhausding) (Executive Member of the [Internet Society Switzerland](https://www.isoc.ch))
   * [Prof. Dr. Andrea Herrmann](https://www.akad.de/fernstudium/akad-university/akad-forum-2019/speaker/dr-habil-andrea-herrmann/) (Professor at the [AKAD University](https://www.akad.de); [Herrmann & Ehrlich](http://www.herrmann-ehrlich.de))
   * [Hernâni Marques](https://vecirex.net) (Council Member [p≡p foundation](https://pep.foundation))
   * [Pirate Party CH](https://www.piratenpartei.ch/)
   * [Peter Schaar](https://peter-schaar.de) (Chairman of the [European Academy for Freedom of Information and Data Protection (EAID)](https://www.eaid-berlin.de), former [German Federal Commissioner for Data Protection and Freedom of Information](https://www.bfdi.bund.de) (2003-2013))
   * [#TEAMHUMAN](https://teamhuman.ch)
   * [Marina Weisband](https://aula-blog.website/team/marina-weisband) (Publicist, Artist)
   * [Peter Welchering](https://www.welchering.de) (Journalist)
