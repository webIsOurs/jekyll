---
layout: "empty"
---
# Privacy Policy

Privacy Policy of the Initiative #webIsOurs - Status: October 20th 2019

1. The initiative #webIsOurs, represented by its initiator Kai Bösefeldt (more details: see imprint), informs by this privacy policy according to art. 13 of the European General Data Protection Regulation (GDPR) that it collects and processes personal data about the following persons:

    * Signatory of the initiative #webIsOurs,
    * Users of the website of the initiative #webIsOurs

2. This data will be used solely to verify signatures and to inform supporters of the progress of the campaign via the #webIsOurs website.

    The data will not be processed for other purposes not compatible with the purpose of the survey.

    The personal data of the signatories will be processed exclusively in Germany.

3. The following data will be processed regularly in connection with the signing:

    * Surname, first name
    * E-mail address
    * Additional information (free text)

    This data will not be passed on to third parties without the consent of the members concerned. They will be deleted by 12-31-2020 at the latest.

4. Personal data required for the provision of the website and the archiving of e-mails may be collected and/or stored via service providers (e-mail providers, web hosts). If data for the #webIsOurs initiative is processed by a service provider, this ensures that the data remains adequately protected and is not misused.

5. Only the initiators of the #webIsOurs initiative as well as the service provider entrusted with the processing may access the data of the signatories. Third parties may only be granted access after explicit consent. Only signatories who begin their signature e-mail in the subject line with the keyword _Public_ may be publicly named as signatories on the website of the #webIsOurs Initiative with surname, first name and Additional information (free text).

6. The persons mentioned in point 1 have the right to request information about the data stored about their person (art. 15 GDPR). They may also request the rectification, cancellation or limitation of the processing of these data (Art. 15 - 18 GDPR) and object to the processing of these data for reasons arising from their particular situation in accordance with Art. 21 GDPR. Furthermore, they can complain to the State Commissioner for Privacy and Freedom of Information Mecklenburg-Vorpommern, Schloss Schwerin, Lennéstraße 1, 19053 Schwerin, or at their local data protection authority, about the processing of their data by the #webIsOurs Initiative, if they are of the opinion that this violates data protection provisions.

7. Each time a user accesses the #webIsOurs Initiative website and each time a file is retrieved, technical information may be collected from visitors to the website, including IP addresses, in order to ensure the security and integrity of the website and the service. See the Web Hosting Privacy Statement at the footer for more information.

8. The legal basis for the processing of the signatories' data is their explicit consent pursuant to art. 6 para. 1 lit. a GDPR. The data of the persons who use the website of the #webIsOurs Initiative will be processed on the basis of Art. 6 para. 1 lit. f) GDPR. The legitimate interest in the processing pursuant to Art. 6 para. 1 lit. f) consists in the provision of the website and the information of the public about the initiative.

    Where data processing is carried out with the consent of the data subjects, Art. 6 para. 1 lit. a) GDPR is relevant. Such consent may be revoked at any time with effect for the future.

9. This privacy policy shall be effective from 12 October 2019.

The data protection regulations of the [European Academy for Freedom of Information and Data Protection (EAID)](https://www.eaid-berlin.de/datenschutzerklaerung-2/) served as a basis for these data protection regulations.

[Back](?lang=en)
