---
layout: "empty"
---
# Let’s reclaim the Web!<sup id="Heading">[*](#footnote)</sup>

## A Manifesto for the Self-Empowerment of Internet Users


### The Internet in 2019

The internet has become a virtual home for many people. They are cultivating friendships and contacts, are seeking and giving advice, are discussing about art, culture and personal matters, are sharing photos, working together, shopping, playing, making appointments. In short: they communicate.

However, few commercial platforms are attracting more and more users. This gives them a monopoly position and precarious power by collecting the most personal data and using it profitably. That is their business. Users, however, are faced with the choice of either entrusting their data to these platforms or being excluded from them and thus from a significant part of their social lives.

The knowledge the companies running these platforms accumulate, means power, bestowed by the users. This knowledge enables those companies to manipulate people in a targeted way far beyond the internet, be it to persuade them to buy certain products, or to influence their behavior in the political arena. This instrument of power is available not only to those companies themselves, but also to anyone who pays for it or, like government authorities who can exert pressure on those companies.

For responsible companies, too, the following applies: board members may be replaced, companies may be acquired, guidelines and legal framework conditions may change. For these reasons, centralized platforms are generally not trustworthy. Reliable trust can only be achieved by technically assured privacy.


### Vision: The Internet of the Future

Today, the internet is already decentralized and cooperative, resistant to centralism, coercion and censorship. Every one can create their own web presence and be free and self-determined masters of their data.

The liberal and democratic Internet of the future, however, also has technology that is suitable for everyday use. It technically implements the confidentiality of communication in the sense of privacy by design as a matter of course. The future internet users are aware of the value of personal data and treat it with care. This way, they protect themselves and others from current and future dangers. Because they know that data that seems harmless today may be highly problematic tomorrow.


### Manifesto

We, the signatories of this manifesto, are firmly convinced that the Internet can and must be owned by the users. This requires trustworthy communication technology, which:
   
   * Guarantees the privacy and rights of users by technical means.
   * Does not require the users to make any compromise in terms of performance and function.
   * Is developed under an approved free and open source license.
   * Is solely run decentrally and collectively by the users.
   * Guarantees secure data protection for content and metadata.
   * Is very easy to install and to use.
   * Provides interoperability with other platforms and a quick and easy migration of profiles.

We call on all Internet users to organize themselves with the aim of establishing trustworthy communication technologies and to use them widely!

We call on all software developers to make trustworthy communication technology available!

We call on all decision makers to promote trustworthy communication technology!

We call on all teachers and artists to create awareness for the handling of personal data!

**Let‘s take matters into our own hands!**

<sup id="footnote" style="font-size:small">This is preliminary translation of the german original text. It may be amended or replaced by a better one. Suggestions for better formulations are welcome. [↩](#Heading)</sup>

{% include initiators_en.md %}
