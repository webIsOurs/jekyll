---
layout: "empty"
---
# Datenschutzordnung

Datenschutzordnung der Initiative #webIsOurs – Stand: 20. Oktober 2019

1. Die Initiative #webIsOurs, vertreten durch ihren Initiator Kai Bösefeldt (nähere Angaben: Siehe Impressum), informiert durch diese Datenschutzordnung nach Art. 13 der Europäischen Datenschutz-Grundverordnung (DS-GVO) darüber, dass sie personenbezogene Daten über folgende Personen bei diesen erhebt und verarbeitet:

    * Unterzeichner der Initiative #webIsOurs,
    * Nutzerinnen und Nutzer der Internetseite der Initiative #webIsOurs.

2. Diese Daten werden ausschließlich dazu verwendet, die Unterschriften zu belegen und die Unterstützer über die Website der Initiative #webIsOurs über den Fortgang der Kampagne zu informieren.

    Zu anderen, nicht mit dem Erhebungszweck vereinbarenden Zwecken, werden die Daten nicht verarbeitet.

    Die personenbezogenen Daten der Unterzeichner werden ausschließlich in Deutschland verarbeitet.

3. Im Zusammenhang mit der Unterzeichnung werden regelmäßig die folgenden Daten verarbeitet:

    * Name, Vorname
    * E-Mail-Adresse
    * Zusatzangabe (Freitext)

    Diese Daten werden nicht ohne Einwilligung der betroffenen Mitglieder an Dritte weitergegeben. Sie werden spätestens am 31.12.2020 gelöscht.

4. Für die Bereitstellung des Webangebots und die Archivierung der E-Mails können für diese Zwecke erforderliche personenbezogene Daten über Serviceanbieter (E-Mail-Anbieter, Webhoster) erhoben und/oder gespeichert werden. Soweit Daten für die #webIsOurs Initiative von einem Serviceanbieter verarbeitet werden, stellt dieser sicher, dass die Daten angemessen geschützt bleiben und nicht zweckentfremdet werden.

5. Auf die Daten der Unterzeichner dürfen nur die Initiatoren der #webIsOurs Initiative sowie der mit der Abwicklung betraute Serviceanbieter zugreifen. Dritten kann Zugriff nach ausdrpcklicher Erlaubnis gewährt werden. Nur Unterzeichner die ihre Unterzeihnungs-E-Mail im Betreff mit dem Schlüsselwort _Public_ beginnen, dürfen mit Familienname, Vorname und Zusatzangabe öffentlich auf der Website der #webIsOurs Initiative als Unterzeichner genannt werden.

6. Die in Ziff. 1 genannten Personen haben das Recht, Auskunft über die zu ihrer Person gespeicherten Daten zu verlangen (Art. 15 DS-GVO). Sie können außerdem die Berichtigung, Löschung oder Einschränkung der Verarbeitung dieser Daten verlangen (Art. 15 – 18 DS-GVO) und der Datenverarbeitung aus Gründen, die sich aus ihrer besonderen Situation ergeben, nach Art. 21 DS-GVO widersprechen. Des weiteren können sie sich beim Landesbeauftragten für Datenschutz und Informationsfreiheit Mecklenburg-Vorpommern, Schloss Schwerin, Lennéstraße 1, 19053 Schwerin, über die Verarbeitung ihrer Daten durch die #webIsOurs Initiative beschweren, wenn sie der Auffassung sind, dass diese gegen datenschutzrechtliche Bestimmungen verstößt.

7. Bei jedem Zugriff eines Nutzers oder einer Nutzerin auf das Internet-Angebot der #webIsOurs Initiative und bei jedem Abruf einer Datei können technische Informationen von Besuchern der Website gesammelt werden, einschließlich der IP-Adressen, um die Sicherheit und Integrität der Website und des Dienstes zu gewährleisten. Näheres kann der Datenschutzerklärung des Webhosters in der Fußzeile entnommen werden.

8. Rechtsgrundlage für die Verarbeitung der Daten der Unterzeichner ist deren explizites Einverständnis gemäß Art. 6 Abs. 1 lit. a DS-GVO. Die Daten der Personen, die das Internet-Angebot der #webIsOurs Initiative nutzen, werden auf der Grundlage von Art. 6 Abs. 1 lit. f) DS-GVO verarbeitet. Das berechtigte Interesse an der Verarbeitung gem. Art. 6 Abs. 1 lit. f) besteht in der Erbringung des Internet-Angebots und der Information der Öffentlichkeit über die Initiative.

    Soweit die Datenverarbeitung mit Einwilligung der Betroffenen erfolgt, ist Art. 6 Abs. 1 lit. a) DS-GVO einschlägig. Die Einwilligung kann jederzeit mit Wirkung für die Zukunft widerrufen werden.

9. Diese Datenschutzordnung tritt am 12. Oktober 2019 in Kraft.

Als Vorlage für diese Datenschutzordnung diente die Datenschutzordung der [Europäischen Akademie für Informationsfreiheit und Datenschutz e.V. (EAID)](https://www.eaid-berlin.de/datenschutzerklaerung-2/)

[Zurück](?lang=de)
