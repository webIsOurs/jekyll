---
layout: "empty"
---
# Holen wir uns das Web zurück!

## Ein Manifest zur Selbstermächtigung der Internet-Nutzer\*innen

### Das Internet im Jahre 2019

Das Internet ist für viele Menschen eine virtuelle Heimat geworden. Sie pflegen dort Freundschaften und Kontakte, sie suchen und geben Rat, diskutieren über Kunst, Kultur und Persönliches, teilen Fotos, arbeiten zusammen, kaufen ein, spielen, verabreden sich. Kurz: Sie kommunizieren.

Allerdings ziehen wenige attraktive, kommerzielle Plattformen immer mehr Nutzer\*innen an. Sie erhalten damit eine Monopolstellung und bedenkliche Macht, indem sie persönlichste Daten ansammeln und gewinnbringend nutzen. Das ist ihr Geschäft. Die Nutzer\*innen stehen indes vor der Wahl, entweder den Betreibern ihre Daten an­zuvertrauen, oder von den Plattformen und damit von einem signifikanten Teil ihres sozialen Umfeldes ausge­schlossen zu werden.

Das angesammelte Wissen der Plattformbetreiber ist Macht, verliehen von den Nutzer\*innen. Dieses Wissen er­möglicht den Betreibern, weit über das Internet hinaus Menschen zielgerichtet zu manipulieren, sei es um sie zum Kauf von Produkten zu bewegen oder um ihr Verhalten im politischen Raum zu beeinflussen. Dieses Machtinstru­ment steht neben den Betreibern selbst jedem zur Verfügung, der dafür zahlt, oder, wie staatliche Stellen, Druck auf die Betreiber ausüben kann.

Auch für verantwortungsvolle Betreiber gilt: Vorstände können wechseln, Firmen aufgekauft, Richtlinien und ge­setzliche Rahmenbedingungen geändert werden. Darum sind zentralisierte Plattformen grundsätzlich nicht ver­trauenswürdig. Zuverlässig vertrauen kann man nur auf technisch sichergestellten Datenschutz.


### Die Vision: Das Internet der Zukunft

Das Internet ist heute schon dezentral und kooperativ angelegt, resistent gegen Zentralismus, Zwang und Zensur. Jede\*r kann dort eine eigene WebPräsenz gestalten und frei und selbstbestimmt Herr über seine Daten sein.

Das freiheitliche, demokratische Internet der Zukunft verfügt aber zusätzlich über alltagstaugliche Technologie, die Vertraulichkeit der Kommunikation im Sinne von Privacy by Design ganz selbstverständlich technisch reali­siert. Denn die Internetnutzer\*innen der Zukunft sind sich des Wertes persönlicher Daten bewusst und gehen sorg­fältig damit um. Dadurch schützen sie sich selbst und ihre Mitmenschen vor aktuellen und zukünftigen Gefahren. Denn sie wissen, dass Daten, die heute harmlos erscheinen, morgen hoch problematisch sein können.


### Manifest

Wir, die Unterzeichner\*innen dieses Manifests, sind der festen Überzeugung, dass das Internet durch die Nutzer\*innen in Besitz genommen werden kann und muss.
Dazu braucht es vertrauenswürdige Kommunikationstechnologie, welche:

   * Die Privatsphäre und die Rechte der Nutzer\*innen auf technischem Wege definitiv sicherstellt.
   * In Leistung und Funktion den Nutzer\*nnen keine Kompromisse abverlangt.
   * Unter einer anerkannten Free and Open Source Lizenz entwickelt wird.
   * Dezentral, kollektiv und ausschließlich von den Nutzer\*innen selbst betrieben wird.
   * Sicheren Datenschutz bei Inhalts- und Metadaten gewährleistet.
   * Sehr einfach zu installieren und zu bedienen ist.
   * Interoperabilität mit anderen Plattformen und einen Umzug von Profilen schnell, einfach und überzeu­gend ermöglicht.

Wir rufen alle Internet-Nutzer\*innen auf, sich mit dem Ziel der Etablierung vertrauenswürdiger Kommunikations­technologien zu organisieren und diese breit zu nutzen!

Wir rufen alle Software-Entwickler\*innen auf, vertrauenswürdige Kommunikationstechnologie verfügbar zu ma­chen!

Wir rufen alle Entscheidungsträger\*innen auf, vertrauenswürdige Kommunikationstechnologie zu fördern!

Wir rufen alle Lehrenden und Kunstschaffenden auf, Bewusstsein zu schaffen für den Umgang mit persönlichen Daten!

**Nehmen wir unsere Geschicke selbst in die Hand!**

{% include initiators_de.md %}
