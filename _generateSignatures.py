#!/usr/bin/python

import sys
import re
import os.path
from os import listdir
from os.path import isfile, join

def getSignatures(path):
  signatureMails =[f for f in listdir(path) if isfile(join(path, f))]
  signatureMails.sort(key=lambda x: os.path.getmtime(join(path, x)))
  signatureMails =[f.replace("%HTTPS%", "https://") for f in signatureMails]
  return [re.split("( - )|(\\.eml)", f)[0] for f in signatureMails]

def getPublic(signs):
  return [f[7:].strip() for f in signs if f.startswith("Public:")]

signatures = getSignatures("../signMails")
signatures.reverse()
vipSignatures = getSignatures("../signMailsVIP")
publicSignatures = getPublic(signatures)
publicVIPSignatures = getPublic(vipSignatures)

nonPublicSignatures = str(len(signatures) + len(vipSignatures) - len(publicSignatures) - len(publicVIPSignatures))
publicSignsatures = str(len(publicSignatures) + len(publicVIPSignatures))
print("Signatures: " + publicSignsatures + " / " + nonPublicSignatures);
scriptFile = open("signatures/script.html", "w")
scriptFile.write("function getNonPublicSignatures(text) { return text + \": \" + " + nonPublicSignatures + "; }\n")
scriptFile.write("function getPublicSignatures(text) { return text + \" (\" + " + publicSignsatures + " + \"):\"; }\n")
scriptFile.close()

signaturesToOutput = 10
signaturesFile = open("signatures/signatures.md", "w")
signaturesFile.write("---\n")
signaturesFile.write("---\n")
if len(sys.argv) == 1:
  for sig in publicVIPSignatures:
    signaturesFile.write("* " + sig + "\n")
    print("* " + sig)
  for sig in publicSignatures:
    signaturesFile.write("* " + sig + "\n")
    if 0 < signaturesToOutput:
      print("* " + sig)
      signaturesToOutput -= 1
else:
  signaturesFile.write("_Signatures omitted_\n")
signaturesFile.close()


